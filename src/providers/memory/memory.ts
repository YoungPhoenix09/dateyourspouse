import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';

export class Memory {
  
  constructor(public caption:string = "", public description:string = "", public img:string = "", public id:number = null, public memType:string = null, public date:string = null) {
    if (!this.date) {
      let currDate = new Date();
      let day = currDate.getDate().toString();
      let month = (currDate.getMonth() + 1).toString();
      let year = currDate.getFullYear().toString();
      
      if (day.length === 1) {
        day = "0" + day;
      }
      
      if (month.length === 1) {
        month = "0" + month;
      }
      
      this.date =  year + "-" + month + "-" + day;
    }
  }
}

export interface MemoryData {
  currentIdx:number;
  memories:Memory[];
}

@Injectable()
export class MemoryProvider {
  memoryData:Promise<MemoryData>;

  constructor(private storage:Storage) {
    //Check if memory data has been established
    this.memoryData = this.storage.get("memoryData")
    .then((memoryData) => {
      if (!memoryData) {
        //Init data if not present
        return this.initMemoryData();
      } else {
        return memoryData;
      }
    });
  }
  
  initMemoryData():Promise<MemoryData> {
    let memoryData:MemoryData = {
      currentIdx: 1,
      memories: []
    };

    //Save data
    return this.storage.set("memoryData", memoryData);
  }
  
  getMemory(id:number):Promise<Memory> {
    return this.memoryData
    .then((memoryData:MemoryData) => {
      //Find memory
      let foundMemory:Memory = memoryData.memories.find((memory:Memory) => {
        return memory.id === id;
      });
      
      //Return memory
      return foundMemory
    });
  }
  
  getMemories():Promise<Memory[]> {
    return this.memoryData
    .then((memoryData:MemoryData) => {
      return memoryData.memories;
    });
  }
  
  saveMemory(memory:Memory):Promise<Memory> {
    return this.memoryData
    .then((memoryData:MemoryData) => {
      //Set memory id
      memory.id = memoryData.currentIdx;
      
      //Add memory to list of memories and sort by id
      memoryData.memories.push(memory);
      memoryData.memories.sort((mem1, mem2) => {
        return mem1.id - mem2.id;
      });
      
      //Increment current index
      memoryData.currentIdx++;
      
      //Save data
      this.memoryData = this.storage.set("memoryData", memoryData);
      
      return memory;
    });
  }
  
  deleteMemory(memory:Memory):Promise<Memory> {
    return this.memoryData
    .then((memoryData:MemoryData) => {
      //Get memories
      let memories:Memory[] = memoryData.memories;
      
      //Get index of memory to be deleted
      let idx:number = memories.findIndex((foundMemory) => {
        return memory.id === foundMemory.id;
      });
      
      //Remove index from array of memories
      memories.splice(idx, 1);
      memoryData.memories = memories;
      
      //Save state of memories
      this.memoryData = this.storage.set("memoryData", memoryData);
      
      return memory;
    });
  }

}
