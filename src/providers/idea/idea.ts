import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';

export class dateIdea {
  id: number;
  img: string;
  caption: string;
  footerMsg: string;
  isUsed:boolean;
  
  constructor(id:number, img:string = "", caption: string = "", footerMsg: string = "", isUsed:boolean = false) {
    this.id = id;
    this.img = img;
    this.caption = caption;
    this.footerMsg = footerMsg;
    this.isUsed = isUsed;
  }
}

/*
  Generated class for the Idea provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class IdeaProvider {
  storage:Storage;
  currentIdea:dateIdea;
  dateIdeas:Promise<dateIdea[]>;
  
  constructor(storage: Storage) {
    this.storage = storage;
    
    this.dateIdeas = this.prepareData();
  }
  
  getIdea():Promise<dateIdea> {
    //Get list of unused ideas
    return this.dateIdeas
      .then((ideas:dateIdea[]) => {
      let unusedIdeas:dateIdea[] = ideas.filter((idea:dateIdea) => {
          //Return all ideas that are unused
          return !idea.isUsed;
        });

        if (unusedIdeas.length === 0) {
          //Reset all isUsed values to false
          ideas.forEach((idea:dateIdea) => {
            idea.isUsed = false;
          });

          unusedIdeas = ideas;
        }

        //Get number of ideas
        let ideaCnt:number = unusedIdeas.length;

        //Get random number within range of ideas
        let rdmIdx: number = Math.floor(Math.random() * ideaCnt);

        //Update stored data
        let ideaId:number = unusedIdeas[rdmIdx].id;
        let ideaIdx = ideas.findIndex((idea:dateIdea) => {
          //Return idea that has the right id
          return idea.id === ideaId;
        });
        ideas[ideaIdx].isUsed = true;
        this.storage.set("ideas", ideas);

        //Return current idea
        return unusedIdeas[rdmIdx];
      })
    .then((idea:dateIdea) => {
      //Return promise of storing idea
      return this.storage.set("currentIdea", idea);
    })
    .then((idea:dateIdea) => {
      //Set currentIdea
      this.currentIdea = idea;
      
      //Return currentIdea
      return this.currentIdea;
    });
  }
  
  prepareData():Promise<dateIdea[]> {
    //Check data storage
    return this.storage.get("ideas")
    .then((ideas:dateIdea[]) => {
      //Callback to return ideas
      function returnIdeas(ideas:dateIdea[]):dateIdea[] {
        console.log("Data initialized");
        return ideas;
      }
      
      //Error callback
      function initErr(err) {
        console.log("Failed to initialize data:" + err);
      }
      
      if (!ideas || ideas.length !== 9) {
        //initialize data for idea
        let dateIdeas = [
          new dateIdea(1, "assets/img/dance.jpg","Dance", "Fast or slow, get on the flo'!"),
          new dateIdea(2, "assets/img/massage.jpg","Massage", "Bust out the oils and incense!"), 
          new dateIdea(3, "assets/img/flowers.jpg","Flowers", "Red roses, blue violets and all that!"),
          new dateIdea(4, "assets/img/make-dinner.jpg","Make A Special Dinner", "Dust off the apron and make them a treat!"),
          new dateIdea(5, "assets/img/Dinner-Date.jpg","Out to Dinner", "Doesn't have to be fancy...but it could be!"),
          new dateIdea(6, "assets/img/movie-night.jpg","Movie Night", "Share popcorn and a movie with the one you love! Let them pick, or a surprise flick!"),
          new dateIdea(7, "assets/img/Dinner-Date.jpg","Take Time to Encourage", "Small, but effective!  Let them know how much you care and why!"),
          new dateIdea(8, "assets/img/love-note.jpg","Leave A Note", "Place a thoughtful note somewhere they will see it.  Leaving a chocolate or two could make it better!"),
          new dateIdea(9, "assets/img/flirt.jpg","Flirt!", "Whether witty, sassy, or corny; it keeps the fire burning!")
        ];
        
        return this.storage.set("ideas", dateIdeas)
          .then(returnIdeas, initErr);
      } else {
        return this.storage.get("ideas")
          .then(returnIdeas, initErr);
      }
    });
  }

}

/*
CREATE TABLE IF NOT EXISTS memories 
(
  memoryDate NUMERIC NOT NULL,
  header TEXT NOT NULL,
  memoryImg BLOB,
  notes TEXT
);

DROP TABLE IF EXISTS date_ideas;
CREATE TABLE IF NOT EXISTS date_ideas
(
  dateImg TEXT NOT NULL,
  caption TEXT NOT NULL,
  footerMsg TEXT NOT NULL,
  used NUMERIC NOT NULL
*/