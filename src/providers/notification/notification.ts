import { Injectable } from '@angular/core';
import { LocalNotifications } from '@ionic-native/local-notifications';

/*
  Generated class for the NotificationProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class NotificationProvider {
  notifyMsg:string = "Check for a new idea for your spouse!";
  notifyTitles:string[] = [
    "Keep the fire buring!",
    "Need an idea?",
    "Do something nice!"
  ];

  constructor(public notify:LocalNotifications) {
    //Setup notification clear and/or click callback
    notify.on("clear", () => {
      this.scheduleNotification();
    });
    notify.on("click", () => {
      this.scheduleNotification();
    });
  }
  
  scheduleNotification(days:number = Math.floor((Math.random() * 3) + 1 )) {
    let numOfDays:number = days * (24 * 60 * 60 * 1000);
    let afterNumOfDays:Date = new Date(Date.now() + numOfDays);
    let titleIdx:number = Math.floor(Math.random() * this.notifyTitles.length);
    
    this.notify.schedule({
      title: this.notifyTitles[titleIdx],
      text: this.notifyMsg,
      at: afterNumOfDays
    });
  }

}
