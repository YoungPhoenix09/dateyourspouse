export class dateIdea {
  img: Object;
  caption: string;
  footerMsg: string;
  
  constructor(img:string = "", caption: string = "", footerMsg: string = "") {
    this.img = img;
    this.caption = caption;
    this.footerMsg = footerMsg;
  }
}

export class dateGenerator {
  idea: dateIdea;
  usedIdeas: dateIdea[];
  dateIdeas: dateIdea[];
  
  constructor() {
    this.idea = new dateIdea();
    
    this.usedIdeas = [];
    
    this.dateIdeas = [
      new dateIdea("/assets/img/dance.jpg","Dance", "Fast or slow, get on the flo'!"),
      new dateIdea("/assets/img/massage.jpg","Massage", "Bust out the oils, incense!"), 
      new dateIdea("/assets/img/flowers.jpg","Flowers", "Red roses, blue violets and all that!"),
      new dateIdea("/assets/img/make-dinner.jpg","Make A Special Dinner", "Dust off the apron and make them a treat!"),
      new dateIdea("/assets/img/Dinner-Date.jpg","Out to Dinner", "Doesn't have to be fancy...but it could be!"),
      new dateIdea("/assets/img/movie-night.jpg","Movie Night", "Share popcorn and a movie with the one you love! Let them pick, or a surprise flick!"),
      new dateIdea("/assets/img/Dinner-Date.jpg","Take Time to Encourage", "Small, but effective!  Let them know how much you care and why!"),
      new dateIdea("/assets/img/love-note.jpg","Leave A Note", "Place a thoughtful note somewhere they will see it.  Leaving a chocolate or two could make it better!"),
      new dateIdea("/assets/img/flirt.jpg","Flirt!", "Whether witty, sassy, or corny; it keeps the fire burning!")
    ];
  }

  pickDate() {
    //Check if all idea have been used
    if (this.dateIdeas.length === 0) {
      this.dateIdeas = this.usedIdeas;
      this.usedIdeas = [];
    }
    
    //Get number of ideas
    let ideaCnt: number = this.dateIdeas.length;
    
    //Get random number within range of ideas
    let rdmIdx: number = Math.floor(Math.random() * ideaCnt); 
    
    //Store idx of used ideas
    this.idea = this.dateIdeas[rdmIdx];
    
    //Remove from idea list and add to used idea list
    this.usedIdeas.push(this.idea);
    this.dateIdeas.splice(rdmIdx, 1);
  }
}