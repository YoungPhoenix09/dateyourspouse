import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { NotificationProvider } from '../providers/notification/notification';

import { HomePage } from '../pages/home/home';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private droidPermissions: AndroidPermissions, public notify:NotificationProvider) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      
      //Set a date 10 seconds after current date
      let after10Secs = new Date();
      after10Secs.setSeconds(after10Secs.getSeconds() + 10);
      
      //Schedule notification
      this.notify.scheduleNotification();
      
      if (platform.is('android')) {
        this.droidPermissions.checkPermission(this.droidPermissions.PERMISSION.CAMERA)
        .then(() => console.log("Camera permission granted."))
        .catch(() => {
          this.droidPermissions.requestPermission(this.droidPermissions.PERMISSION.CAMERA);
        });
      }
    });
    
  }
}

