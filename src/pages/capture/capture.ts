import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, ToastController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Memory, MemoryProvider } from '../../providers/memory/memory';
import { SocialSharing } from '@ionic-native/social-sharing';

/**
 * Generated class for the CapturePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-capture',
  templateUrl: 'capture.html'
})
export class CapturePage {

  cameraOptions:CameraOptions;
  memory:Memory;
  isSharing:boolean;
  shareCaption:string;
  shareMessage:string;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public vwCtrl: ViewController, public camera:Camera, public alert:AlertController, public toast:ToastController, public memProvider:MemoryProvider, public social:SocialSharing) {
    this.isSharing = false;
    this.cameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true
    };
    
    let sentMemory = navParams.get("memory");
    if (!!sentMemory) {
      this.memory = sentMemory;
    } else {
      this.memory = new Memory();
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CapturePage');
  }
  
  close() {
    if (this.isSharing) {
      this.isSharing = false;
    } else {
      this.vwCtrl.dismiss();
    }
    
  }
  
  photoCaptureErr(errMsg) {
    console.log("Image capture failed: " + errMsg);
  }
  
  takePhoto() {
    this.cameraOptions.sourceType = this.camera.PictureSourceType.CAMERA;
    this.camera.getPicture(this.cameraOptions)
    .then((imgURI) => {
      this.memory.img = imgURI;
    }, this.photoCaptureErr);
  }
  
  browsePhoto() {
    this.cameraOptions.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;
    this.camera.getPicture(this.cameraOptions)
      .then((imgURI) => {
      this.memory.img = imgURI;
    }, this.photoCaptureErr);
  }
  
  validateMemory() {
    let alert;
    
    if (!this.memory.date) {
      alert = this.alert.create({
        title: "Woops!",
        subTitle: "You forgot to add a date.",
        buttons: ['Ok']
      });
    } else if (!this.memory.caption) {
      alert = this.alert.create({
        title: "Woops!",
        subTitle: "You forgot to add a caption.",
        buttons: ['Ok']
      });
    }else if (!this.memory.description) {
      alert = this.alert.create({
        title: "Woops!",
        subTitle: "You forgot to add a description.",
        buttons: ['Ok']
      });
    }
    
    if (!!alert) {
      alert.present();
    } else {
      this.saveMemory();
    }
  }
  
  validatePost() {
    let alert;
    
    if (!this.shareCaption) {
      alert = this.alert.create({
        title: "Woops!",
        subTitle: "You forgot to add a caption for your post.",
        buttons: ['Ok']
      });
    } else if (!this.shareMessage) {
      alert = this.alert.create({
        title: "Woops!",
        subTitle: "You forgot to add a message for your post.",
        buttons: ['Ok']
      });
    }
    
    if (!!alert) {
      alert.present();
    } else {
      this.postMemory();
    }
  }
  
  saveMemory() {
    //Set img if necessary
    if (!this.memory.img) {
      this.memory.img = this.navParams.get("idea").img;
    }
    
    //Set memory type
    this.memory.memType = this.navParams.get("idea").caption;
    
    //Save memory
    this.memProvider.saveMemory(this.memory)
    .then((memory:Memory) => {
      this.memory = memory;
      
      //Show success toast
      let toast = this.toast.create({
        message: "Memory saved!",
        duration: 3000,
        position: "bottom",
        cssClass: "savedToast"
      });
      toast.present();
    });
  }
  
  shareButtonClick() {
    this.isSharing = true;
  }
  
  postMemory() {
    //Post to social media
    this.social.share(this.shareMessage, this.shareCaption, this.memory.img, null)
    .then((retVal) => {
      //Show success toast
      let toast = this.toast.create({
        message: "Post has been sent!",
        duration: 3000,
        position: "bottom",
        cssClass: "sentToast"
      });
      toast.present();
    }, (err) => {
      console.log("Failed to post: " + err);
      
      //Show error toast
      let toast = this.toast.create({
        message: "Uh oh, something went wrong...",
        duration: 3000,
        position: "bottom",
        cssClass: "errToast"
      });
      toast.present();
    });
    
    //Reset post share values
    this.shareCaption = "";
    this.shareMessage = "";
    
    //Return to regular view
    this.isSharing = false;
  }

}
