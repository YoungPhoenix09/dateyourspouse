import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
//import { dateGenerator } from '../../app/dateGenerator.component';
import { MemoriesPage } from '../memories/memories';
import { CapturePage } from '../capture/capture';
import { IdeaProvider, dateIdea } from '../../providers/idea/idea';
import { NotificationProvider } from '../../providers/notification/notification';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  //dateGen:dateGenerator = new dateGenerator();
  ideaProvider:IdeaProvider;
  memoriesPage;
  capturePage;
  idea:dateIdea;
  
  constructor(public navCtrl: NavController, public modalCtrl: ModalController, ideaProvider: IdeaProvider, public notify: NotificationProvider) {
    this.ideaProvider = ideaProvider;
    this.memoriesPage = MemoriesPage;
    this.capturePage = CapturePage;
    this.idea = new dateIdea(0);
    this.notify.scheduleNotification();
  }
  
  showMemories() {
    this.navCtrl.push(this.memoriesPage);
  }
  
  showCapture() {
    //Setup idea to be passed to modal
    let params = {idea: this.idea};
    
    //Create and show modal
    let captureModal = this.modalCtrl.create(this.capturePage, params);
    captureModal.present();
  }
  
  doRefresh(refresher) {
    //this.dateGen.pickDate();
    this.ideaProvider.getIdea()
    .then(idea => {
      this.idea = idea;
      this.notify.scheduleNotification();
      refresher.complete();
    }, err => {
      console.log("Failed to refresh: " + err);
      this.notify.scheduleNotification();
      refresher.complete();
    });
  }

}
