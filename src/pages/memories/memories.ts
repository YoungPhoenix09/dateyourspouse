import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ModalController } from 'ionic-angular';
import { Memory, MemoryProvider } from '../../providers/memory/memory';

import { CapturePage } from '../capture/capture';

/**
 * Generated class for the MemoriesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-memories',
  templateUrl: 'memories.html',
})
export class MemoriesPage {
  memories:Memory[];
  capturePage;

  constructor(public navCtrl: NavController, public navParams: NavParams, public memProvider:MemoryProvider, public toast:ToastController, public modalController:ModalController) {
    this.capturePage = CapturePage;
    this.memories = [];
    this.refreshMemories();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MemoriesPage');
  }
  
  showMemoryDetail(memory:Memory) {
    let modal = this.modalController.create(this.capturePage, {memory:memory});
    modal.present();
  }
  
  refreshMemories() {
    this.memProvider.getMemories()
    .then((memories:Memory[]) => {
      this.memories = memories;
    });
  }
  
  deleteMemory(memory:Memory) {
    this.memProvider.deleteMemory(memory)
    .then((memory:Memory) => {
      //Show success toast
      let toast = this.toast.create({
        message: `'${memory.memType}' memory removed!`,
        duration: 3000,
        position: "bottom",
        cssClass: "deletedToast"
      });
      toast.present();
    })
    .then(() => {
      this.refreshMemories();
    });
  }

}
